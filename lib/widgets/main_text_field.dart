import 'package:flutter/material.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MainTextField extends StatefulWidget {
  final Function(String textValue) onTextChanged;
  final bool isNumber;
  final String hintText;
  final IconData icon;
  final int maxLines;
  MainTextField(
      {Key key,
        @required this.hintText,
        this.icon,
        @required this.onTextChanged,this.isNumber, this.maxLines})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MainTextFieldState();
  }
}

class _MainTextFieldState extends State<MainTextField> {
  TextEditingController textController;
  @override
  void initState() {
    super.initState();
    this.textController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      decoration: BoxDecoration(
        color: HColors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: <Widget>[

          TextField(
                keyboardType: widget.isNumber?TextInputType.number:TextInputType.text,
                controller: textController,
                maxLines: widget.maxLines==null?1:widget.maxLines,
                style: TextStyle(fontSize: 16,fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                  contentPadding:
                  const EdgeInsets.only(left: 14.0, bottom: 15, top: 15,right:14),
                  fillColor: HColors.white,
                  hintText: widget.hintText,
                  hintStyle: TextStyle(fontSize: 16, color: HColors.brighterBlack,fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold),
                  border:InputBorder.none,
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: HColors.ColorSecondPrimary,style: BorderStyle.solid,width: 2),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  filled: true,
                  prefixIcon: widget.icon!=null?Icon(
                    widget.icon,
                    color: HColors.ColorSecondPrimary,
                    size: 22.0,
                  ):null,
                ),
                onChanged: (value) {

                  widget.onTextChanged(value);
                },
              )
        ],
      ),
    );
  }


}
