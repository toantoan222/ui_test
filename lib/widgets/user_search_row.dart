import 'package:flutter/material.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/checkbox_group.dart';
import 'package:ui_component/widgets/grouped_buttons_orientation.dart';
import 'package:ui_component/widgets/radio_button_group.dart';

class UserSearchRow extends StatefulWidget {
  final String username;
  final Function onSelected;
  final bool isAssign;
  final List<String> list;
  UserSearchRow(
      {Key key,
      @required this.username,
      @required this.onSelected,
      @required this.isAssign,this.list})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _UserSearchRowState();
  }
}

class _UserSearchRowState extends State<UserSearchRow> {
  List<String> _checked = [];
  String _picked = "Adriana C. Ocampo Uria";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(bottom: 16.0),
      child: Container(
        margin: EdgeInsets.only(top: 15, left: 10, right: 10),
        child: Material(
          elevation: 0,
          borderRadius: BorderRadius.circular(12.0),
          color: Colors.transparent,
          child: Container(
              decoration: BoxDecoration(
                color: HColors.white,
                borderRadius: BorderRadius.circular(10),

              ),
              child: Row(

                children: <Widget>[

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child:  CheckboxGroup(
                            activeColor: HColors.ColorSecondPrimary,
                            orientation: GroupedButtonsOrientation.HORIZONTAL,
                            labelStyle: TextStyle(
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.ColorSecondPrimary),
                            onSelected: (List selected) => setState(() {
                                  _checked = selected;
                                  print(_checked);
                                  widget..onSelected(_checked);
                                }),
                            labels: <String>[widget.username],
                            checked: _checked,
                            itemBuilder: (Checkbox cb, Text txt, int i) {
                              return Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.center,
                                    height: 60,
                                    width: 60,
                                    margin: EdgeInsets.symmetric(vertical: 15,horizontal: 10),
                                    decoration: BoxDecoration(

                                        border:
                                        Border.all(color: HColors.ColorSecondPrimary, width: 3),
                                        borderRadius: new BorderRadius.circular(50),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                'https://images.unsplash.com/photo-1553503991-443a7548f2d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80'),
                                            fit: BoxFit.cover)),
                                  ),
                                  /// Title and rating
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.5,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(widget.username,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontFamily:
                                                Hfonts.PrimaryFontBold)),
                                        Text('Chức vụ',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontFamily:
                                                Hfonts.PrimaryFontRegular)),
                                      ],
                                    ),
                                  ),
                                  cb,

                                  /// Infos
                                ],
                              );
                            },
                          )

                  ),
                ],
              )),
        ),
      ),
    );
  }
}
