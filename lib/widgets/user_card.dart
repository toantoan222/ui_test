import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class UserCard extends StatefulWidget {
  final bool isHome;
  final String name;
  final String role;
  UserCard({@required this.isHome,Key key,@required this.name,@required this.role}):super(key:key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _UserCardState();
  }
}

class _UserCardState extends State<UserCard> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
//      decoration: BoxDecoration(
//        color: HColors.white,
//        borderRadius: BorderRadius.circular(10),
//        boxShadow: [
//          BoxShadow(
//            color: Colors.blueAccent.withOpacity(0.5),
//            blurRadius: 20.0, // has the effect of softening the shadow
//            spreadRadius: 5.0, // has the effect of extending the shadow
//            offset: Offset(
//              5, // horizontal, move right 10
//              5, // vertical, move down 10
//            ),
//          )
//        ],
//      ),
      height: 150,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: <Widget>[
          Container(
            height: 30,
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: HColors.ColorSecondPrimary,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(10)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width*0.45,
                  child: Text('Trang thái: Kích hoạt',style: TextStyle(fontSize: 12,fontFamily: Hfonts.PrimaryFontBold,color: HColors.white),),
                ),
                Container(
                  width: MediaQuery.of(context).size.width*0.4,
                  child: Text('Ngày hết hạn: 28/12/2019',style: TextStyle(fontSize: 12,fontFamily: Hfonts.PrimaryFontBold,color: HColors.white),),
                ),
              ],
            ),
          ),
          Container(
            height: 120,
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                color: HColors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10))),
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.18,
                  height: MediaQuery.of(context).size.width * 0.18,
                  decoration: BoxDecoration(
                      color: HColors.white,
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: HColors.ColorSecondPrimary, width: 3)),
                  child: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: NetworkImage(
                        'https://images.unsplash.com/photo-1553503991-443a7548f2d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80'),
                    backgroundColor: Colors.transparent,
                  ),
                ),
                //SizedBox(width: MediaQuery.of(context).size.width*0.02,),
                Container(
                  width: MediaQuery.of(context).size.width*0.55,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          widget.name,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: Hfonts.PrimaryFontBold,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          widget.role,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          'Số nhóm đang quản trị: 0',
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          'Số nhóm đang tham gia: 4',
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                widget.isHome?IconButton(icon: Icon(FontAwesomeIcons.edit,color: HColors.ColorSecondPrimary,), onPressed: (){
                  Navigator.pushNamed(context, '/editprofile');
                }):SizedBox.shrink(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
