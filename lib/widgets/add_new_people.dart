import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class AddNewPeople extends StatefulWidget{
  final String title;
  final IconData icon;
  AddNewPeople({@required this.title,@required this.icon,Key key}):super(key:key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddNewPeopleState();
  }

}

class _AddNewPeopleState extends State<AddNewPeople>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        print('tap');
        Navigator.pushNamed(context, '/search');
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 40,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: HColors.white,
          borderRadius: BorderRadius.circular(10),

        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child:  Text(
                widget.title,
                style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,color: HColors.hintTextColor,fontSize: 20),
              ),
            ),
            Icon(
              widget.icon,
              color: HColors.ColorSecondPrimary,
            ),
          ],
        ),
      ),
    );
  }
}