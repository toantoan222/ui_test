import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class GroupRowWidget extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GroupRowWidgetState();
  }

}

class _GroupRowWidgetState extends State<GroupRowWidget>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 80,
      //padding: EdgeInsets.symmetric(horizontal: 10),
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: BoxDecoration(
        color: HColors.white,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.black26)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width*0.55,
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text('Tên nhóm',overflow: TextOverflow.ellipsis,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontWeight: FontWeight.bold,fontSize: 16),),
                    ),
                    Container(
                      child: Text('Chủ nhóm',overflow: TextOverflow.ellipsis,style: TextStyle(fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold,fontSize: 14),),
                    ),
                    Container(
                      child: Text('Hiện có n thành viên',overflow: TextOverflow.ellipsis,style: TextStyle(fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold,fontSize: 14),),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}