import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class BuildGridTile extends StatefulWidget {
  final VoidCallback onTap;
  final Widget child;
  BuildGridTile({Key key,@required this.onTap,@required this.child}):super(key:key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BuildGridTileState();
  }
}

class _BuildGridTileState extends State<BuildGridTile> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
        elevation: 14.0,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: InkWell
          (
          // Do onTap() if it isn't null, otherwise do print()
            onTap: widget.onTap != null ? () => widget.onTap() : () { print('Not set yet'); },
            child: widget.child
        )
    );
  }
}
