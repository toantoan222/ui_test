import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class RoundButton extends StatefulWidget{
  final String title;
  final IconData icon;
  final VoidCallback onPressed;
  RoundButton({Key key,@required this.title,@required this.onPressed,@required this.icon}):super(key:key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _RoundButtonState();
  }

}

class _RoundButtonState extends State<RoundButton>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: MediaQuery.of(context).size.width * 0.15,
            decoration: BoxDecoration(
                color: HColors.white,
                border: Border.all(
                    color: HColors.ColorSecondPrimary, width: 3),
//                boxShadow: [
//                  BoxShadow(
//                    color: Colors.blueAccent.withOpacity(0.2),
//                    blurRadius: 20.0,
//                    // has the effect of softening the shadow
//                    spreadRadius: 5.0,
//                    // has the effect of extending the shadow
//                    offset: Offset(
//                      5, // horizontal, move right 10
//                      5, // vertical, move down 10
//                    ),
//                  )
//                ],
                borderRadius: BorderRadius.circular(50)),
            child: IconButton(icon: Icon(widget.icon,color: HColors.ColorSecondPrimary,), onPressed: widget.onPressed,),
        ),
        Text(
          widget.title,
          style: TextStyle(fontFamily: Hfonts.PrimaryFontBold),
        ),
      ],
    );
  }
}