import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class UserProfileCard extends StatefulWidget{
  final String address;
  final String phone;
  final String mobilePhone;
  final String email;
  final String website;
  UserProfileCard({Key key,@required this.address,@required this.phone,@required this.mobilePhone,@required this.email,@required this.website}):super(key:key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _UserProfileCardState();
  }

}

class _UserProfileCardState extends State<UserProfileCard>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
        color: HColors.white,
        borderRadius: BorderRadius.circular(10),

      ),
      height: 230,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 30,
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: HColors.ColorSecondPrimary,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(10)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width*0.45,
                  child: Text('Thông tin tài khoản',style: TextStyle(fontSize: 16,fontFamily: Hfonts.PrimaryFontBold,color: HColors.white),),
                ),
                IconButton(icon: Icon(FontAwesomeIcons.edit,size: 15,color: HColors.white,), onPressed: (){
                  Navigator.pushNamed(context, '/editprofile');
                })
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: HColors.ColorSecondPrimary))),
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child:Text('ĐỊA CHỈ',style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:16,color: HColors.ColorSecondPrimary),),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.place,size: 13,color: HColors.ColorSecondPrimary,),
                    SizedBox(width: 5,),
                    Container(
                      child:Text(widget.address,overflow:TextOverflow.ellipsis,maxLines:2,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:14,color: HColors.black),),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 15,),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: HColors.ColorSecondPrimary))),
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child:Text('LIÊN LẠC',style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:16,color: HColors.ColorSecondPrimary),),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.phone,size: 13,color: HColors.ColorSecondPrimary,),
                    SizedBox(width: 5,),
                    Container(
                      child:Text(widget.phone,overflow:TextOverflow.ellipsis,maxLines:2,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:14,color: HColors.black),),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.phone_iphone,size: 13,color: HColors.ColorSecondPrimary,),
                    SizedBox(width: 5,),
                    Container(
                      child:Text(widget.mobilePhone,overflow:TextOverflow.ellipsis,maxLines:2,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:14,color: HColors.black),),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.solidEnvelope,size: 13,color: HColors.ColorSecondPrimary,),
                    SizedBox(width: 5,),
                    Container(
                      child:Text(widget.email,overflow:TextOverflow.ellipsis,maxLines:2,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:14,color: HColors.black),),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.globe,size: 13,color: HColors.ColorSecondPrimary,),
                    SizedBox(width: 5,),
                    Container(
                      child:Text(widget.website,overflow:TextOverflow.ellipsis,maxLines:2,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize:14,color: HColors.black),),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}