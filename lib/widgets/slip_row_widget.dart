import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';

class SlipRowWidget extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SlipRowWidgetState();
  }

}

class _SlipRowWidgetState extends State<SlipRowWidget>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 80,
      padding: EdgeInsets.symmetric(horizontal: 10),
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: BoxDecoration(
        color: HColors.white,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.black26),
      ),
      child: Row(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.13,
            height: MediaQuery.of(context).size.width * 0.13,
            decoration: BoxDecoration(
                color: HColors.ColorSecondPrimary,
                borderRadius: BorderRadius.circular(50)),
            child: Icon(
              FontAwesomeIcons.solidHandshake,
              color: HColors.white,
              size: MediaQuery.of(context).size.width * 0.05,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width*0.4,
            margin: EdgeInsets.only(left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text('30/05/2019',overflow: TextOverflow.ellipsis,style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontWeight: FontWeight.bold,fontSize: 20),),
                ),
                Container(
                  child: Text('Breda Curry',overflow: TextOverflow.ellipsis,style: TextStyle(fontFamily: Hfonts.PrimaryFontRegular,fontWeight: FontWeight.bold,fontSize: 18),),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }
}