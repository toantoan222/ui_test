import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_component/screen/add_new_group_screen.dart';
import 'package:ui_component/screen/assign_user_to_group_screen.dart';
import 'package:ui_component/screen/demo_screen.dart';
import 'package:ui_component/screen/forgot_pass_screen.dart';
import 'package:ui_component/screen/home_screen.dart';
import 'package:ui_component/screen/search_screen.dart';

import 'screen/edit_profile_screen.dart';
import 'screen/login_screen.dart';
import 'screen/navigation_screen.dart';
import 'screen/one_to_one_screen.dart';
import 'screen/referral_screen.dart';
import 'screen/tyfcb_screen.dart';


class MyApp extends StatelessWidget {

  final app = MaterialApp(
    initialRoute: '/',
    routes: <String, WidgetBuilder>{
      '/': (BuildContext context) => DemoScreen(),
      '/login':(BuildContext context)=>LoginScreen(),
      '/forgotpass':(BuildContext context)=>ForgotPassScreen(),
      '/home':(BuildContext context)=>HomeScreen(),
      '/navigation':(BuildContext context)=>NavigationScreen(),
      '/TYFCB':(BuildContext context)=>TYFCBScreen(),
      '/onetoone':(BuildContext context)=>OneToOneScreen(),
      '/editprofile':(BuildContext context)=>EditProfileScreen(),
      '/referral':(BuildContext context)=>ReferralScreen(),
      '/addgroup':(BuildContext context)=>AddNewGroupScreen(),
      '/assignuser':(BuildContext context)=>AssignUserToGroupScreen(),
      '/search':(BuildContext context)=>SearchScreen(),
    },
  );

  @override
  Widget build(BuildContext context) {
    return app;
  }
}
