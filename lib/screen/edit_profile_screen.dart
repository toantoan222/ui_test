import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/main_text_field.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditProfileScreenState();
  }
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HColors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: HColors.ColorSecondPrimary,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        centerTitle: true,
        title: Text(
          'Sửa thông tin',
          style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,color: HColors.ColorSecondPrimary),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                height: 100,
                width: 100,
                margin: EdgeInsets.symmetric(vertical: 15),
                decoration: BoxDecoration(

                    border:
                        Border.all(color: HColors.ColorSecondPrimary, width: 3),
                    borderRadius: new BorderRadius.circular(50),
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://images.unsplash.com/photo-1553503991-443a7548f2d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80'),
                        fit: BoxFit.cover)),
              ),
            ],
          ),
          CommonButton(
            onPressed: () {},
            backgroundColor: Colors.transparent,
            textColor: HColors.ColorSecondPrimary,
            buttonPadding: 10,
            icon: Icon(FontAwesomeIcons.cameraRetro),
            text: Text(
              'THAY ĐỔI AVATAR',
              style: TextStyle(fontFamily: Hfonts.PrimaryFontBold),
            ),
            width: MediaQuery.of(context).size.width * 0.5,
            radiusValue: 5,
          ),
          MainTextField(
                hintText: 'Địa chỉ',
                icon: Icons.place,
                onTextChanged: (value) {},
                isNumber: true),

         MainTextField(
                hintText: 'Điện thoại bàn',
                icon: Icons.phone,
                onTextChanged: (value) {},
                isNumber: true),
           MainTextField(
                hintText: 'Điện thoại di động',
                icon: Icons.phone_iphone,
                onTextChanged: (value) {},
                isNumber: true),

           MainTextField(
                hintText: 'Địa chỉ email',
                icon: FontAwesomeIcons.solidEnvelope,
                onTextChanged: (value) {},
                isNumber: true),

           MainTextField(
                hintText: 'Website',
                icon: FontAwesomeIcons.globe,
                onTextChanged: (value) {},
                isNumber: true),
          SizedBox(height: 5,),
          CommonButton(
            onPressed: () {
              Navigator.pop(context);
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            buttonPadding: 10,
            radiusValue: 10,
            width: MediaQuery.of(context).size.width*0.9,
            text: Text(
              'Lưu lại',
              style:
              TextStyle(fontFamily: Hfonts.PrimaryFontBold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
