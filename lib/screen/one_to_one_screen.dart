import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/add_new_people.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/grouped_buttons_orientation.dart';
import 'package:ui_component/widgets/main_text_field.dart';
import 'package:ui_component/widgets/radio_button_group.dart';
import 'package:ui_component/widgets/textfield.dart';

class OneToOneScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OneToOneScreenState();
  }
}

class _OneToOneScreenState extends State<OneToOneScreen> {
// Declare this variable
  String _picked = "Chính tôi";
  int selectedRadioTile;
  int selectedRadio;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    selectedRadio = 0;
    selectedRadioTile = 0;
    _dropdownselected = '';
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  final List<String> _dropdownValues = [
    "Adriana C. Ocampo Uria",
    "Albert Einstein",
    "Blaise Pascal",
    "Caroline Herschel",
    "Cecilia Payne-Gaposchkin",
    "Chien-Shiung Wu",
    "Dorothy Hodgkin",
    "Edmond Halley",
    "Edwin Powell Hubble",
    "Elizabeth Blackburn",
    "Enrico Fermi",
    "Erwin Schroedinger",
    "Flossie Wong-Staal",
    "Frieda Robscheit-Robbins",
    "Geraldine Seydoux",
    "Gertrude B. Elion",
    "Ingrid Daubechies",
    "Jacqueline K. Barton",
  ];
  String _dropdownselected;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HColors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: HColors.ColorSecondPrimary,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        centerTitle: true,
        title: Text(
          'ONE TO ONE SLIP',
          style: TextStyle(
              fontFamily: Hfonts.PrimaryFontBold,
              color: HColors.ColorSecondPrimary),
        ),
      ),
      body: ListView(
        children: <Widget>[
          AddNewPeople(
              title: 'Gửi lời cảm ơn tới', icon: FontAwesomeIcons.userCircle),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                padding: const EdgeInsets.only(top: 14.0),
                child: Text("Tố chức bởi",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        fontFamily: Hfonts.PrimaryFontBold,
                        color: HColors.ColorSecondPrimary)),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 2),
                decoration: BoxDecoration(
                  color: HColors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: RadioButtonGroup(
                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                  activeColor: HColors.ColorSecondPrimary,
                  labelStyle: TextStyle(
                      fontFamily: Hfonts.PrimaryFontBold,
                      color: HColors.ColorSecondPrimary),
                  onSelected: (String selected) => setState(() {
                        _picked = selected;
                        print(_picked);
                      }),
                  labels: <String>["Chính tôi", _dropdownselected],
                  disabled:
                      _dropdownselected == "" ? [_dropdownselected] : null,
                  picked: _picked,
                  itemBuilder: (Radio rb, Text txt, int i) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        rb,
                        txt,
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
          MainTextField(
              hintText: 'Địa điểm gặp mặt',
              icon: FontAwesomeIcons.placeOfWorship,
              onTextChanged: (value) {},
              isNumber: true),
          MainTextField(
            hintText: 'Ngày giờ gặp mặt',
            icon: FontAwesomeIcons.businessTime,
            onTextChanged: (value) {},
            isNumber: false,
          ),
          MainTextField(
            hintText: 'Nhập nội dung',
            icon: null,
            onTextChanged: (value) {},
            isNumber: false,
            maxLines: 6,
          ),
          SizedBox(
            height: 5,
          ),
          CommonButton(
            onPressed: () {
              Navigator.pop(context);
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            buttonPadding: 10,
            radiusValue: 10,
            width: MediaQuery.of(context).size.width * 0.9,
            text: Text(
              'Lưu lại',
              style:
                  TextStyle(fontFamily: Hfonts.PrimaryFontBold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
