import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DemoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: 1,
          itemBuilder: (context, index) {
            if(index==0){
              return getTitle('Tour', (){
                Navigator.pushNamed(context,  '/login');
              });
            }
          }),
    );
  }

  Widget getTitle(String title, VoidCallback callback) {
    return InkWell(onTap: callback, child: Container(
        height: 48,
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey, width: 1.0))
        ),
        child: Text(title)));
  }
}
