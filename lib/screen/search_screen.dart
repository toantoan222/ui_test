import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/checkbox_group.dart';
import 'package:ui_component/widgets/grouped_buttons_orientation.dart';
import 'package:ui_component/widgets/input_tags.dart';
import 'package:ui_component/widgets/main_text_field.dart';
import 'package:ui_component/widgets/textfield.dart';
import 'package:ui_component/widgets/user_search_row.dart';

class SearchScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<String> _inputTags = [];
  final List<String> _dropdownValues = [
    "Adriana C. Ocampo Uria",
    "Albert Einstein",
    "Blaise Pascal",
    "Caroline Herschel",
    "Cecilia Payne-Gaposchkin",
    "Chien-Shiung Wu",
    "Dorothy Hodgkin",
    "Edmond Halley",
    "Edwin Powell Hubble",
    "Elizabeth Blackburn",
    "Enrico Fermi",
    "Erwin Schroedinger",
    "Flossie Wong-Staal",
    "Frieda Robscheit-Robbins",
    "Geraldine Seydoux",
    "Gertrude B. Elion",
    "Ingrid Daubechies",
    "Jacqueline K. Barton",
  ];
  var items = List<String>();
  @override
  void initState() {
    items.addAll(_dropdownValues);
    super.initState();
  }

  void filterSearchResults(String query) {
    List<String> dummySearchList = List<String>();
    dummySearchList.addAll(_dropdownValues);
    if (query.isNotEmpty) {
      List<String> dummyListData = List<String>();
      dummySearchList.forEach((item) {
        if (item.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(_dropdownValues);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: HColors.ColorSecondPrimary,
            ),
            onPressed: () => Navigator.pop(context)),
        title: Text(
          'Chọn người dùng',
          style: TextStyle(color:HColors.ColorSecondPrimary, fontFamily: Hfonts.PrimaryFontBold),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.check,
                color: HColors.ColorSecondPrimary,
              ),
              onPressed: () => Navigator.pop(context)),
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            MainTextField(
                  hintText: 'Gõ tên người dùng bạn muốn tìm',
                  icon: Icons.search,
                  onTextChanged: (value) {
                    filterSearchResults(value);
                  },
                  isNumber: false),

//            _inputTags.length!=0?Container(
//              width: MediaQuery.of(context).size.width,
//              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
//              padding: EdgeInsets.symmetric(horizontal: 10),
//              decoration: BoxDecoration(
//                color: HColors.white,
//                borderRadius: BorderRadius.circular(10),
//                boxShadow: [
//                  BoxShadow(
//                    color: Colors.blueAccent.withOpacity(0.3),
//                    blurRadius: 20.0, // has the effect of softening the shadow
//                    spreadRadius: 5.0, // has the effect of extending the shadow
//                    offset: Offset(
//                      5, // horizontal, move right 10
//                      5, // vertical, move down 10
//                    ),
//                  )
//                ],
//              ),
//              child:
//              InputTags(
//                tags: _inputTags,
//                columns: 10,
//                fontSize: 15,
//                symmetry: false,
//                iconBackground: HColors.ColorSecondPrimary,
//                color: HColors.ColorSecondPrimary,
//                lowerCase: true,
//                autofocus: false,
//                popupMenuBuilder: (String tag){
//                  return <PopupMenuEntry>[
//                    PopupMenuItem(
//                      child: Text(tag,
//                        style: TextStyle(
//                            color: Colors.black87,fontWeight: FontWeight.w800,fontFamily: Hfonts.PrimaryFontBold
//                        ),
//                      ),
//                      enabled: false,
//                    ),
//                    PopupMenuDivider(),
//                    PopupMenuItem(
//                      value: 1,
//                      child: Row(
//                        children: <Widget>[
//                          Icon(Icons.content_copy,size: 18,),
//                          Text(" Copy text",style: TextStyle(
//                              color: Colors.black87,fontFamily: Hfonts.PrimaryFontBold
//                          ),),
//                        ],
//                      ),
//                    ),
//                    PopupMenuItem(
//                      value: 2,
//                      child: Row(
//                        children: <Widget>[
//                          Icon(Icons.delete,size: 18),
//                          Text(" Remove",style: TextStyle(
//                              color: Colors.black87,fontFamily: Hfonts.PrimaryFontBold
//                          ),),
//                        ],
//                      ),
//                    )
//                  ];
//                },
//                popupMenuOnSelected: (int id,String tag){
//                  switch(id){
//                    case 1:
//                      Clipboard.setData( ClipboardData(text: tag));
//                      break;
//                    case 2:
//                      setState(() {
//                        _dropdownValues.add(tag);
//                        _inputTags.remove(tag);
//                      });
//                  }
//                },
//                textFieldHidden: true,
//                //boxShadow: [],
//                //offset: -2,
//                //padding: EdgeInsets.only(left: 11),
//                //margin: EdgeInsets.symmetric(horizontal: 20, vertical: 6),
//                //iconPadding: EdgeInsets.all(5),
//                //iconMargin: EdgeInsets.only(right:5,left: 2),
//                //borderRadius: BorderRadius.all(Radius.elliptical(50, 5)),
//                onDelete: (tag) {
//                  setState(() {
//                    _dropdownValues.add(tag);
//                    _inputTags.remove(tag);
//                  });
//                  print('delete');
//                },
//                //onInsert: (tag) => print(tag),
//
//              ),
//            ):SizedBox.shrink(),
            Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    physics: ScrollPhysics(),
                    itemCount: items.length,
                    itemBuilder: (context, index) {
                      return UserSearchRow(username: items[index],onSelected: (selected){
                        setState(() {
                          _inputTags.addAll(selected);
                        });
                      }, isAssign: false,list: items,);
                    }))
          ],
        ),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }
}
