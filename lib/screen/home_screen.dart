import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/build_grid_tile.dart';
import 'package:ui_component/widgets/user_card.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'invitation_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  ScrollController _scrollController;
  TabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = TabController(vsync: this, length: 2);
    _scrollController = new ScrollController(initialScrollOffset: 0.0);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: <Widget>[
          UserCard(isHome: true, name: 'Tên Chủ tài khoản', role: 'Chức vụ'),
          new Container(
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
            height: 40,
            decoration: BoxDecoration(
              color: HColors.ColorSecondPrimary,
              borderRadius: BorderRadius.circular(10),
//            boxShadow: [
//              BoxShadow(
//                color: Colors.blueAccent.withOpacity(0.3),
//                blurRadius: 20.0, // has the effect of softening the shadow
//                spreadRadius: 5.0, // has the effect of extending the shadow
//                offset: Offset(
//                  5, // horizontal, move right 10
//                  5, // vertical, move down 10
//                ),
//              )
//            ],
            ),
            child: new TabBar(
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: HColors.white,
              controller: _controller,
              labelStyle: TextStyle(fontFamily: Hfonts.PrimaryFontBold),
              tabs: [
                new Tab(
                  icon: null,
                  text: '12 tháng qua',
                ),
                new Tab(
                  icon: null,
                  text: 'Từ trước đến nay',
                ),
              ],
            ),
          ),
          Expanded(
            child: new TabBarView(

              controller: _controller,
              children: <Widget>[_build12month(), _buildLifeTime()],
            ),
          )
        ],
      ),
    );
  }

  _build12month() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: StaggeredGridView.count(
        shrinkWrap: true,
        controller: _scrollController,
        crossAxisCount: 3,
        crossAxisSpacing: 14.0,
        mainAxisSpacing: 14,
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        children: <Widget>[
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      height: 42,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.shynnyRed,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Gặp trực tiếp',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '46',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.shynnyRed),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/onetoone');
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      height: 42,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.deepPurple,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Cơ hội đã trao',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '50',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.deepPurple),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/referral');
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      height: 42,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.deepSkyBlue,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Số TYFCB đã gửi',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '55904',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: Hfonts.PrimaryFontBold,
                              color: HColors.deepSkyBlue,
                            ),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/TYFCB');
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      height: 42,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.weirdGreen,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Số khách thăm',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '2',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.weirdGreen),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => InvitaionScreen(
                            isHome: true,
                          )));
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      height: 42,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.deepRed,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Cơ hội đã nhận',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                          child: Text(
                        '46',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: HColors.deepRed,
                          fontSize: 14,
                          fontFamily: Hfonts.PrimaryFontBold,
                        ),
                      )),
                    )
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/referral');
            }),
          ),
          Center(
            child: _buildTile(
              Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 42,
                    padding: EdgeInsets.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                        color: HColors.tangerine,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12))),
                    child: Center(
                        child: Text(
                      'Doanh thu',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: Hfonts.PrimaryFontBold,
                          fontSize: 16,
                          color: HColors.white),
                    )),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: Text(
                          '100000000000đ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: HColors.tangerine,
                            fontSize: 11,
                            fontFamily: Hfonts.PrimaryFontBold,
                          ),
                        ),
                      ))
                ],
              ),
            ),
          ),
        ],
        staggeredTiles: [
          StaggeredTile.extent(1, 100),
          StaggeredTile.extent(1, 100.0),
          StaggeredTile.extent(1, 100.0),
          StaggeredTile.extent(1, 100),
          StaggeredTile.extent(1, 100.0),
          StaggeredTile.extent(1, 100.0),
        ],
      ),
    );
  }

  _buildLifeTime() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: StaggeredGridView.count(
        shrinkWrap: true,
        controller: _scrollController,
        crossAxisCount: 3,
        crossAxisSpacing: 14.0,
        mainAxisSpacing: 14,
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        children: <Widget>[
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.shynnyRed,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Gặp trực tiếp',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '46',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.shynnyRed),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/onetoone');
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.deepPurple,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Cơ hội đã trao',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '50',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.deepPurple),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/referral');
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.deepSkyBlue,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Số TYFCB đã gửi',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '55904',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: Hfonts.PrimaryFontBold,
                              color: HColors.deepSkyBlue,
                            ),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/TYFCB');
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.weirdGreen,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Số khách thăm',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                          child: Text(
                            '2',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Hfonts.PrimaryFontBold,
                                color: HColors.weirdGreen),
                          ),
                        ))
                  ],
                ), onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => InvitaionScreen(
                            isHome: true,
                          )));
            }),
          ),
          Center(
            child: _buildTile(
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      decoration: BoxDecoration(
                          color: HColors.deepRed,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              topRight: Radius.circular(12))),
                      child: Text(
                        'Cơ hội đã nhận',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: Hfonts.PrimaryFontBold,
                            fontSize: 16,
                            color: HColors.white),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                        '46',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: HColors.deepRed,
                          fontSize: 14,
                          fontFamily: Hfonts.PrimaryFontBold,
                        ),
                      ),
                    )
                  ],
                ), onTap: () {
              Navigator.pushNamed(context, '/referral');
            }),
          ),
          Center(
            child: _buildTile(
              Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    padding: EdgeInsets.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                        color: HColors.tangerine,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12))),
                    child: Text(
                      'Doanh thu',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: Hfonts.PrimaryFontBold,
                          fontSize: 16,
                          color: HColors.white),
                    ),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: Text(
                          '100000000000đ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: HColors.tangerine,
                            fontSize: 11,
                            fontFamily: Hfonts.PrimaryFontBold,
                          ),
                        ),
                      ))
                ],
              ),
            ),
          ),
        ],
        staggeredTiles: [
          StaggeredTile.extent(1, 100),
          StaggeredTile.extent(1, 100.0),
          StaggeredTile.extent(1, 100.0),
          StaggeredTile.extent(1, 100),
          StaggeredTile.extent(1, 100.0),
          StaggeredTile.extent(1, 100.0),
        ],
      ),
    );
  }
}

Widget _buildTile(Widget child, {Function() onTap}) {
  return Material(
//      elevation: 5,
      borderRadius: BorderRadius.circular(12.0),
//      shadowColor: Color(0x802196F3),
      child: InkWell(
          // Do onTap() if it isn't null, otherwise do print()
          onTap: onTap != null
              ? () => onTap()
              : () {
                  print('Not set yet');
                },
          child: child));
}
