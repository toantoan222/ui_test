import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'dart:ui' as ui;

import 'package:ui_component/widgets/textfield.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.9), BlendMode.dstATop),
              image: AssetImage('assets/images/bg.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: new BackdropFilter(
            filter: new ui.ImageFilter.blur(sigmaX: 3, sigmaY: 3),
            child: new Container(
              //you can change opacity with color here(I used black) for background.
              decoration:
                  new BoxDecoration(color: Colors.black.withOpacity(0.2)),
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: _buildBody(),
        ),
      ],
    );
  }

  _buildBody() {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 35,
          ),
          Center(
            child: Image.asset(
              'assets/images/bussiness.png',
              width: MediaQuery.of(context).size.width * 0.6,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 35,
          ),
          MNTextField(
              hintText: 'Nhập email',
              icon: FontAwesomeIcons.envelopeOpen,
              onTextChanged: (value) {},
              isPassword: false),
          MNTextField(
              hintText: 'Nhập mật khẩu',
              icon: FontAwesomeIcons.key,
              onTextChanged: (value) {},
              isPassword: true),
          CommonButton(
            onPressed: () {
              Navigator.pushNamed(context, '/navigation');
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            buttonPadding: 15,
            radiusValue: 10,
            width: MediaQuery.of(context).size.width*0.75,
            text: Text(
              'Đăng nhập',
              style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize: 18),
            ),
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: new Text(
                  "Quên mật khẩu?",
                  style: TextStyle(
                      fontFamily: Hfonts.PrimaryFontBold,
                      color: HColors.white,
                      fontSize: 18,
                      fontWeight:FontWeight.bold
                  ),
                  textAlign: TextAlign.end,
                ),
                onPressed: () => {
                  //_showForgotPassSentDialog()
                  Navigator.pushNamed(context, '/forgotpass'),
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
