import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/user_card.dart';
import 'package:ui_component/widgets/user_profile_card.dart';

class AccountScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AccountScreenState();
  }
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      body: ListView(
        children: <Widget>[
          UserCard(isHome: false, name: 'Tên Chủ Tài Khoản', role: 'Chức vụ'),
          UserProfileCard(
              address: '35 example st, example city',
              phone: '1234567',
              mobilePhone: '123456789',
              email: 'example@email.com',
              website: 'abc.com'),
          CommonButton(
            onPressed: () {
              Navigator.pop(context);
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            buttonPadding: 10,
            width: MediaQuery.of(context).size.width * 0.9,
            radiusValue: 5,
            icon: Icon(FontAwesomeIcons.signOutAlt,color: HColors.white,),
            text: Text('Đăng xuất',style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize: 20),),
          ),
        ],
      ),
    );
  }
}
