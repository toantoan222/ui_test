import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'dart:ui' as ui;

import 'package:ui_component/widgets/textfield.dart';

class ForgotPassScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ForgotPassScreenState();
  }
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.9), BlendMode.dstATop),
              image: AssetImage('assets/images/bg.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: new BackdropFilter(
            filter: new ui.ImageFilter.blur(sigmaX: 3, sigmaY: 3),
            child: new Container(
              //you can change opacity with color here(I used black) for background.
              decoration:
              new BoxDecoration(color: Colors.black.withOpacity(0.2)),
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: _buildBody(),
        ),
      ],
    );
  }

  _buildBody() {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 35,
          ),
          Center(
            child: Image.asset(
              'assets/images/forgot.png',
              width: MediaQuery.of(context).size.width * 0.6,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 35,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 18),
            width: MediaQuery.of(context).size.width * 0.4,
            child: Text(
              'Hãy nhập email của bạn, sau đó chúng tôi sẽ gửi cho bạn một email hướng dẫn lấy lại mật khẩu',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,color: HColors.white, fontSize: 20,fontFamily: Hfonts.PrimaryFontBold),
            ),
          ),
          SizedBox(
            height: 35,
          ),
          MNTextField(
              hintText: 'Nhập email',
              icon: FontAwesomeIcons.envelopeOpen,
              onTextChanged: (value) {},
              isPassword: false),
          CommonButton(
            onPressed: () {},
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            buttonPadding: 15,
            radiusValue: 10,
            width: MediaQuery.of(context).size.width*0.75,
            text: Text(
              'Lấy lại mật khẩu',
              style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize: 18),
            ),
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: new Text(
                  "Quay lại trang đăng nhập",
                  style: TextStyle(
                      fontFamily: Hfonts.PrimaryFontBold,
                      color: HColors.white,
                      fontSize: 18,
                      fontWeight:FontWeight.bold
                  ),
                  textAlign: TextAlign.end,
                ),
                onPressed: () => {
                  Navigator.pop(context),
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
