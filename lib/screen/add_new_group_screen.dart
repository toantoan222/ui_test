import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/screen/assign_user_to_group_screen.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/add_new_people.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/main_text_field.dart';

class AddNewGroupScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddNewGroupScreenState();
  }
}

class _AddNewGroupScreenState extends State<AddNewGroupScreen> {
  final List<String> _dropdownValues = [
    "Adriana C. Ocampo Uria",
    "Albert Einstein",
    "Blaise Pascal",
    "Caroline Herschel",
    "Cecilia Payne-Gaposchkin",
    "Chien-Shiung Wu",
    "Dorothy Hodgkin",
    "Edmond Halley",
    "Edwin Powell Hubble",
    "Elizabeth Blackburn",
    "Enrico Fermi",
    "Erwin Schroedinger",
    "Flossie Wong-Staal",
    "Frieda Robscheit-Robbins",
    "Geraldine Seydoux",
    "Gertrude B. Elion",
    "Ingrid Daubechies",
    "Jacqueline K. Barton",
  ];
  String _dropdownselected;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dropdownselected = '';
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HColors.ColorSecondPrimary,
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          'THÊM NHÓM MỚI',
          style: TextStyle(fontFamily: Hfonts.PrimaryFontBold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          MainTextField(
                hintText: 'Tên nhóm',
                icon: FontAwesomeIcons.layerGroup,
                onTextChanged: (value) {},
                isNumber: true),
          AddNewPeople(
            icon: Icons.supervised_user_circle,
            title: 'Chỉ định Admin nhóm',
          ),
          SizedBox(
            height: 5,
          ),
          CommonButton(
            onPressed: () {
              Navigator.pop(context);
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            buttonPadding: 10,
            radiusValue: 10,
            width: MediaQuery.of(context).size.width * 0.9,
            text: Text(
              'Lưu lại',
              style:
                  TextStyle(fontFamily: Hfonts.PrimaryFontBold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
