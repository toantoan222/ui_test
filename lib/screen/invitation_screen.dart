import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/add_new_people.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/main_text_field.dart';

class InvitaionScreen extends StatefulWidget {
  final bool isHome;

  InvitaionScreen({@required this.isHome, Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _InvitationScreenState();
  }
}

class _InvitationScreenState extends State<InvitaionScreen> {
  final List<String> _dropdownValues = [
    "Adriana C. Ocampo Uria",
    "Albert Einstein",
    "Blaise Pascal",
    "Caroline Herschel",
    "Cecilia Payne-Gaposchkin",
    "Chien-Shiung Wu",
    "Dorothy Hodgkin",
    "Edmond Halley",
    "Edwin Powell Hubble",
    "Elizabeth Blackburn",
    "Enrico Fermi",
    "Erwin Schroedinger",
    "Flossie Wong-Staal",
    "Frieda Robscheit-Robbins",
    "Geraldine Seydoux",
    "Gertrude B. Elion",
    "Ingrid Daubechies",
    "Jacqueline K. Barton",
  ];
  String _dropdownselected;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dropdownselected = '';
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      appBar: widget.isHome
          ? AppBar(
              elevation: 0.0,
              backgroundColor: HColors.white,
              leading: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HColors.ColorSecondPrimary,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              title: Text(
                'LỜI MỜI',
                style: TextStyle(
                    fontFamily: Hfonts.PrimaryFontBold,
                    color: HColors.ColorSecondPrimary),
              ),
              actions: <Widget>[
                widget.isHome == false
                    ? IconButton(
                        icon: Icon(FontAwesomeIcons.signOutAlt),
                        onPressed: () {
                          Navigator.pop(context);
                        })
                    : SizedBox.shrink()
              ],
            )
          : null,
      body: ListView(
        children: <Widget>[
          AddNewPeople(title: 'Mời', icon: FontAwesomeIcons.phoneSquare),
          MainTextField(
              hintText: 'Họ',
              icon: Icons.looks_one,
              onTextChanged: (value) {},
              isNumber: true),
          MainTextField(
              hintText: 'Tên',
              icon: Icons.looks_two,
              onTextChanged: (value) {},
              isNumber: true),
          MainTextField(
              hintText: 'Công ty',
              icon: FontAwesomeIcons.placeOfWorship,
              onTextChanged: (value) {},
              isNumber: true),
          MainTextField(
              hintText: 'email',
              icon: FontAwesomeIcons.solidEnvelope,
              onTextChanged: (value) {},
              isNumber: true),
          MainTextField(
            hintText: 'Tin nhắn',
            icon: null,
            onTextChanged: (value) {},
            isNumber: false,
            maxLines: 6,
          ),
          SizedBox(
            height: 5,
          ),
          CommonButton(
            onPressed: () {},
            backgroundColor: HColors.ColorSecondPrimary,
            width: MediaQuery.of(context).size.width * 0.9,
            textColor: HColors.white,
            buttonPadding: 10,
            radiusValue: 10,
            text: Text(
              'MỜI',
              style:
                  TextStyle(fontFamily: Hfonts.PrimaryFontBold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
