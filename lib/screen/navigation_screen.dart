import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/screen/account_screen.dart';
import 'package:ui_component/screen/group_screen.dart';
import 'package:ui_component/screen/invitation_screen.dart';
import 'package:ui_component/screen/slips_screen.dart';
import 'package:ui_component/styles/styles.dart';

import 'home_screen.dart';

class NavigationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NavigationScreenState();
  }
}

class _NavigationScreenState extends State<NavigationScreen> {
  int currentIndex = 0;
  List<Widget> pages = [
    HomeScreen(),
    SlipScreen(),
    InvitaionScreen(
      isHome: false,
    ),
    GroupScreen(),
    AccountScreen()
  ];
  List<String> title = [
    'TRANG CHÍNH',
    'SLIPS',
    'LỜI MỜI',
    'QUẢN LÝ NHÓM',
        'THÔNG TIN TÀI KHOẢN'
  ];

  bool navBarMode = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HColors.white,
        automaticallyImplyLeading: false,
        title: Text(
          title[currentIndex],
          style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,color: HColors.ColorSecondPrimary),
        ),
      ),
      body: pages[currentIndex],

      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (index){
          setState(() {
            currentIndex=index;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.home,color: HColors.ColorSecondPrimary,),
            icon: Icon(Icons.home,color: HColors.hintTextColor,),
            title: currentIndex==0?Text("Home",style: TextStyle(color: HColors.ColorSecondPrimary,fontFamily: Hfonts.PrimaryFontRegular),):Text("Home",style: TextStyle(color: HColors.hintTextColor,fontFamily: Hfonts.PrimaryFontRegular),),
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.event_note,color: HColors.ColorSecondPrimary,),
            icon: Icon(Icons.event_note,color: HColors.hintTextColor,),
            title: currentIndex==1?Text("Slips",style: TextStyle(color: HColors.ColorSecondPrimary,fontFamily: Hfonts.PrimaryFontRegular),):Text("Slips",style: TextStyle(color: HColors.hintTextColor,fontFamily: Hfonts.PrimaryFontRegular),),
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.insert_invitation,color: HColors.ColorSecondPrimary,),
            icon: Icon(Icons.insert_invitation,color: HColors.hintTextColor,),
            title: currentIndex==2?Text("Invitations",style: TextStyle(color: HColors.ColorSecondPrimary,fontFamily: Hfonts.PrimaryFontRegular),):Text("Invitations",style: TextStyle(color: HColors.hintTextColor,fontFamily: Hfonts.PrimaryFontRegular),),
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.group,color: HColors.ColorSecondPrimary,),
            icon: Icon(Icons.group,color: HColors.hintTextColor,),
            title: currentIndex==3?Text("Groups",style: TextStyle(color: HColors.ColorSecondPrimary,fontFamily: Hfonts.PrimaryFontRegular),):Text("Groups",style: TextStyle(color: HColors.hintTextColor,fontFamily: Hfonts.PrimaryFontRegular),),
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.account_circle,color: HColors.ColorSecondPrimary,),
            icon: Icon(Icons.account_circle,color: HColors.hintTextColor,),
            title: currentIndex==4?Text("Account",style: TextStyle(color: HColors.ColorSecondPrimary,fontFamily: Hfonts.PrimaryFontRegular),):Text("Account",style: TextStyle(color: HColors.hintTextColor,fontFamily: Hfonts.PrimaryFontRegular),),
          ),
        ],

      ),
    );
  }
}
