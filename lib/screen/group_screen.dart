import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/action_widget.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/group_row_widget.dart';
class GroupScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GroupScreenState();
  }
}

class _GroupScreenState extends State<GroupScreen> {
  ScrollController _scrollController;
  List items = getDummyList();
  static List getDummyList() {
    List list = List.generate(10, (i) {
      return "Item ${i + 1}";
    });
    return list;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController = new ScrollController(initialScrollOffset: 0.0);
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      body: _buildBody(),
    );
  }

  _buildBody() {
    return ListView(
      controller: _scrollController,
      children: <Widget>[
        SizedBox(height: 20,),
        CommonButton(
            onPressed: (){
              Navigator.pushNamed(context, '/addgroup');
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
          icon: Icon(FontAwesomeIcons.plusCircle,color: HColors.white,),
            buttonPadding: 10,radiusValue: 10,text: Text('Tạo nhóm mới',style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize: 20),),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Text('Danh sách nhóm của bạn',style: TextStyle(fontFamily: Hfonts.PrimaryFontBold,color: HColors.ColorSecondPrimary,fontSize: 20),),
        ),
        _buildGroupList(),
      ],
    );
  }

  _buildGroupList() {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.transparent,
      child: new ListView.builder(
        shrinkWrap: true,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) {
          return new OnSlide(
            backgroundColor: Colors.transparent,
            items: <ActionItems>[
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(FontAwesomeIcons.userEdit),
                    onPressed: () {

                    },
                    color: HColors.lightBlue,
                  ),
                  onPress: () {
                    Navigator.pushNamed(context, '/assignuser');
                  },
                  backgroudColor: Colors.transparent),
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(FontAwesomeIcons.edit),
                    onPressed: () {},
                    color: HColors.tangerine,
                  ),
                  onPress: () {},
                  backgroudColor: Colors.transparent),
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(FontAwesomeIcons.trash),
                    onPressed: () {},
                    color: HColors.red,
                  ),
                  onPress: () {},
                  backgroudColor: Colors.transparent),
            ],
            child: GroupRowWidget(),
          );
        },
        itemCount: items.length,
      ),
    );
  }
}
