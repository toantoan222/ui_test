import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/action_widget.dart';
import 'package:ui_component/widgets/round_button.dart';
import 'package:ui_component/widgets/slip_row_widget.dart';
import 'package:ui_component/widgets/user_card.dart';

class SlipScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SlipScreenState();
  }
}

class _SlipScreenState extends State<SlipScreen>
    with SingleTickerProviderStateMixin {
  List items = getDummyList();
  ScrollController _scrollController;
  TabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController = new ScrollController(initialScrollOffset: 0.0);
    _controller = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      body: _buildBody(),
    );
  }

  _buildGivenList() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: new ListView.builder(
        shrinkWrap: true,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) {
          return new OnSlide(
            backgroundColor: Colors.transparent,
            items: <ActionItems>[
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(FontAwesomeIcons.eye),
                    onPressed: () {},
                    color: HColors.lightBlue,
                  ),
                  onPress: () {},
                  backgroudColor: Colors.transparent),
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(FontAwesomeIcons.edit),
                    onPressed: () {},
                    color: HColors.tangerine,
                  ),
                  onPress: () {},
                  backgroudColor: Colors.transparent),
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(FontAwesomeIcons.trash),
                    onPressed: () {},
                    color: HColors.red,
                  ),
                  onPress: () {},
                  backgroudColor: Colors.transparent),
            ],
            child: SlipRowWidget(),
          );
        },
        itemCount: items.length,
      ),
    );
  }

  static List getDummyList() {
    List list = List.generate(10, (i) {
      return "Item ${i + 1}";
    });
    return list;
  }

  _buildBody() {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 90,
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                color: HColors.white,
                borderRadius: BorderRadius.circular(10),
//                boxShadow: [
//                  BoxShadow(
//                    color: Colors.blueAccent.withOpacity(0.3),
//                    blurRadius: 20.0, // has the effect of softening the shadow
//                    spreadRadius: 5.0, // has the effect of extending the shadow
//                    offset: Offset(
//                      5, // horizontal, move right 10
//                      5, // vertical, move down 10
//                    ),
//                  )
//                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  RoundButton(title: 'TYFCB', icon: FontAwesomeIcons.handshake,onPressed: (){
                    Navigator.pushNamed(context, '/TYFCB');
                  },),
                  RoundButton(title: 'REFERRAL', icon: FontAwesomeIcons.solidStickyNote,onPressed: (){
                    Navigator.pushNamed(context, '/referral');
                  },),
                  RoundButton(title: '1-2-1', icon: FontAwesomeIcons.userFriends,onPressed: (){
                    Navigator.pushNamed(context, '/onetoone');
                  },),
                ],
              ),
            )
          ],
        ),
        new Container(
          height: 60,
          margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
          decoration: BoxDecoration(
            color: HColors.ColorSecondPrimary,
            borderRadius: BorderRadius.circular(10),
//            boxShadow: [
//              BoxShadow(
//                color: Colors.blueAccent.withOpacity(0.3),
//                blurRadius: 20.0, // has the effect of softening the shadow
//                spreadRadius: 5.0, // has the effect of extending the shadow
//                offset: Offset(
//                  5, // horizontal, move right 10
//                  5, // vertical, move down 10
//                ),
//              )
//            ],
          ),
          child: new TabBar(
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorColor: HColors.white,
            controller: _controller,
            tabs: [
              new Tab(
                icon: const Icon(FontAwesomeIcons.handsHelping,size: 20,),
                text: 'Đã gửi',
              ),
              new Tab(
                icon: const Icon(FontAwesomeIcons.handHoldingUsd,size: 20,),
                text: 'Đã nhận',
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            child: new TabBarView(
              controller: _controller,
              physics: const NeverScrollableScrollPhysics(),
              children: <Widget>[
                _buildGivenList(),
                _buildGivenList(),
              ],
            ),
          ),
        )
      ],
    );
  }
}
