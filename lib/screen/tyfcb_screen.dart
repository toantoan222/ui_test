import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ui_component/styles/styles.dart';
import 'package:ui_component/widgets/add_new_people.dart';
import 'package:ui_component/widgets/common_button.dart';
import 'package:ui_component/widgets/grouped_buttons_orientation.dart';
import 'package:ui_component/widgets/main_text_field.dart';
import 'package:ui_component/widgets/radio_button_group.dart';
import 'package:ui_component/widgets/textfield.dart';

class TYFCBScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TYFCBScreenState();
  }
}

class _TYFCBScreenState extends State<TYFCBScreen> {
// Declare this variable
  String _picked = "New";
  String _picked2 = "Inside";
  int selectedRadioTile;
  int selectedRadio;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    selectedRadio = 0;
    selectedRadioTile = 0;
    _dropdownselected = '';
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  final List<String> _dropdownValues = [
    "Adriana C. Ocampo Uria",
    "Albert Einstein",
    "Blaise Pascal",
    "Caroline Herschel",
    "Cecilia Payne-Gaposchkin",
    "Chien-Shiung Wu",
    "Dorothy Hodgkin",
    "Edmond Halley",
    "Edwin Powell Hubble",
    "Elizabeth Blackburn",
    "Enrico Fermi",
    "Erwin Schroedinger",
    "Flossie Wong-Staal",
    "Frieda Robscheit-Robbins",
    "Geraldine Seydoux",
    "Gertrude B. Elion",
    "Ingrid Daubechies",
    "Jacqueline K. Barton",
  ];
  String _dropdownselected;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: HColors.bgColor,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HColors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: HColors.ColorSecondPrimary,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        centerTitle: true,
        title: Text(
          'TYFCB',
          style: TextStyle(
              fontFamily: Hfonts.PrimaryFontBold, color: HColors.ColorSecondPrimary),
        ),
      ),
      body: ListView(
        children: <Widget>[
          AddNewPeople(title: 'Gửi lời cảm ơn tới', icon: FontAwesomeIcons.userCircle),
          MainTextField(
                hintText: 'Số tiền',
                icon: FontAwesomeIcons.moneyBill,
                onTextChanged: (value) {},
                isNumber: true),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    padding: const EdgeInsets.only(top: 14.0),
                    child: Text(
                      "Bussiness Type",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          fontFamily: Hfonts.PrimaryFontBold,
                          color: HColors.ColorSecondPrimary),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 2),
                    decoration: BoxDecoration(
                      color: HColors.white,
                      borderRadius: BorderRadius.circular(10),

                    ),
                    child: RadioButtonGroup(
                      orientation: GroupedButtonsOrientation.HORIZONTAL,
                      activeColor: HColors.ColorSecondPrimary,
                      labelStyle: TextStyle(
                          fontFamily: Hfonts.PrimaryFontBold,
                          color: HColors.ColorSecondPrimary),
                      onSelected: (String selected) => setState(() {
                            _picked = selected;
                            print(_picked);
                          }),
                      labels: <String>[
                        "New",
                        "Repeat",
                      ],
                      picked: _picked,
                      itemBuilder: (Radio rb, Text txt, int i) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            rb,
                            txt,
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    padding: const EdgeInsets.only(top: 14.0),
                    child: Text(
                      "Referral Type",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          fontFamily: Hfonts.PrimaryFontBold,
                          color: HColors.ColorSecondPrimary),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 2),
                    decoration: BoxDecoration(
                      color: HColors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: RadioButtonGroup(
                      orientation: GroupedButtonsOrientation.HORIZONTAL,
                      activeColor: HColors.ColorSecondPrimary,
                      labelStyle: TextStyle(
                          fontFamily: Hfonts.PrimaryFontBold,
                          color: HColors.ColorSecondPrimary),
                      onSelected: (String selected) => setState(() {
                            _picked2 = selected;
                            print(_picked2);
                          }),
                      labels: <String>["Inside", "Outside", "Tider 3+"],
                      picked: _picked2,
                      itemBuilder: (Radio rb, Text txt, int i) {
                        return Row(
                          children: <Widget>[
                            rb,
                            txt,
                          ],
                        );
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
          MainTextField(
              hintText: 'Nhập nội dung',
              icon: null,
              onTextChanged: (value) {},
              isNumber: false,
              maxLines: 6,
            ),
          SizedBox(
            height: 5,
          ),
          CommonButton(
            onPressed: () {
              Navigator.pop(context);
            },
            backgroundColor: HColors.ColorSecondPrimary,
            textColor: HColors.white,
            width: MediaQuery.of(context).size.width * 0.9,
            buttonPadding: 10,
            radiusValue: 10,
            text: Text(
              'Lưu lại',
              style:
                  TextStyle(fontFamily: Hfonts.PrimaryFontBold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
