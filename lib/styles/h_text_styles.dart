




import 'package:flutter/material.dart';
import 'package:ui_component/styles/styles.dart';

class HTextStyles{
  static const TextStyle bigText= TextStyle(fontFamily: Hfonts.PrimaryFontBold,fontSize: 16,fontWeight: FontWeight.bold);
  final TextStyle smallText= TextStyle(fontFamily: Hfonts.PrimaryFontRegular,fontSize: 14,fontWeight: FontWeight.bold);
}
